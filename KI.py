from time import time
import pandas as pd
import numpy as np
import gensim
from gensim.models import KeyedVectors
import re
import io
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split #real name: scikit-learn

import itertools
import datetime
import tensorflow as tf

from gensim.models.wrappers import FastText

from tensorflow.keras.utils import Sequence
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Embedding, LSTM, Lambda
from tensorflow.keras import layers
import tensorflow.keras.backend as K
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping, Callback

from kerastuner.tuners import RandomSearch
from kerastuner.engine.hyperparameters import HyperParameters

import numpy as np
from keras_transformer import get_model

import tensorflow as tf
physical_devices = tf.config.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(physical_devices[0], enable=True)

# File paths
EMBEDDING_FILE = 'GoogleNews-vectors-negative300.bin.gz'
#EMBEDDING_FILE = 'cc.en.300'
#EMBEDDING_FILE = 'wiki-news-300d-1M.vec'
SNLI_TRAIN = 'snli_1.0_train.csv'
QUORA_TRAIN = 'quora_train.csv'

###### SNLI Train
snli = pd.read_csv(SNLI_TRAIN, sep=";")
gold_label = snli["gold_label"]

binary_scores = []
for i in range(len(gold_label)):
    if gold_label[i] == "entailment":
        binary_scores.append(1)
    else:
        binary_scores.append(0)
        
series = pd.Series(binary_scores, name="binary_scores")
snli = pd.concat([snli, series], axis=1)
snli = snli[snli.gold_label != "neutral"]



###### QUORA Train

quora = pd.read_csv(QUORA_TRAIN, sep=";")
snli = snli.append(quora, ignore_index=True)
snli = snli.sample(frac=1).reset_index(drop=True)

snli.to_csv("snli_binary.csv", sep=";", columns=["sentence_A", "sentence_B", "binary_scores"])
SICK = "snli_binary.csv"

def text_to_word_list(text):
    ''' Pre process and convert texts to a list of words '''
    text = str(text)
    text = text.lower()

    # Clean the text
    text = re.sub(r"[^A-Za-z0-9^,!.\/'+-=]", " ", text)
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"can't", "cannot ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r",", " ", text)
    text = re.sub(r"\.", " ", text)
    text = re.sub(r"!", " ! ", text)
    text = re.sub(r"\/", " ", text)
    text = re.sub(r"\^", " ^ ", text)
    text = re.sub(r"\+", " + ", text)
    text = re.sub(r"\-", " - ", text)
    text = re.sub(r"\=", " = ", text)
    text = re.sub(r"'", " ", text)
    text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
    text = re.sub(r":", " : ", text)
    text = re.sub(r" e g ", " eg ", text)
    text = re.sub(r" b g ", " bg ", text)
    text = re.sub(r" u s ", " american ", text)
    text = re.sub(r"\0s", "0", text)
    text = re.sub(r" 9 11 ", "911", text)
    text = re.sub(r"e - mail", "email", text)
    text = re.sub(r"j k", "jk", text)
    text = re.sub(r"\s{2,}", " ", text)

    text = text.split()

    return text

# Load training and test set
SICK = "snli_binary.csv"
train_df = pd.read_csv(SICK, sep=";")

stops = set(stopwords.words('english'))

# Prepare embedding
print("Load Embedding File")
vocabulary = dict()
inverse_vocabulary = ['<unk>']  # '<unk>' will never be used, it is only a placeholder for the [0, 0, ....0] embedding
word2vec = KeyedVectors.load_word2vec_format(EMBEDDING_FILE, binary=True)

questions_cols = ['sentence_A', 'sentence_B']
print("Iterate over all Questions")
# Iterate over the questions only of both training and test datasets
for dataset in [train_df]:
    for index, row in dataset.iterrows():

        # Iterate through the text of both questions of the row
        for question in questions_cols:

            q2n = []  # q2n -> question numbers representation
            for word in text_to_word_list(row[question]):
                # Check for unwanted words
                if word in stops and word not in word2vec.vocab:
                    continue

                if word not in vocabulary:
                    vocabulary[word] = len(inverse_vocabulary)
                    q2n.append(len(inverse_vocabulary))
                    inverse_vocabulary.append(word)
                else:
                    q2n.append(vocabulary[word])

            # Replace questions as word to question as number representation
            dataset.at[index, question] = q2n
            
embedding_dim = 300
embeddings = 1 * np.random.randn(len(vocabulary) + 1, embedding_dim)  # This will be the embedding matrix
embeddings[0] = 0  # So that the padding will be ignored

# Build the embedding matrix
for word, index in vocabulary.items():
    if word in word2vec.vocab:
        embeddings[index] = word2vec.word_vec(word)

del word2vec
np.save("embeding_matrix_snli", embeddings)
np.save("vocabulary", vocabulary) 

max_seq_length = max(train_df.sentence_A.map(lambda x: len(x)).max(),
                     train_df.sentence_B.map(lambda x: len(x)).max())

# Split to train validation
validation_size = 5000
training_size = len(train_df) - validation_size

X = train_df[questions_cols]
Y = train_df['binary_scores']

X_train, X_validation, Y_train, Y_validation = train_test_split(X, Y, test_size=validation_size)

# Split to dicts
X_train = {'left': X_train.sentence_A, 'right': X_train.sentence_B}
X_validation = {'left': X_validation.sentence_A, 'right': X_validation.sentence_B}

# Convert labels to their numpy representations
Y_train = Y_train.values
Y_validation = Y_validation.values

# Zero padding
for dataset, side in itertools.product([X_train, X_validation], ['left', 'right']):
    dataset[side] = pad_sequences(dataset[side], maxlen=max_seq_length)

# Make sure everything is ok
assert X_train['left'].shape == X_train['right'].shape
assert len(X_train['left']) == len(Y_train)

def build_model():
    def exponent_neg_manhattan_distance(left, right):
        ''' Helper function for the similarity estimate of the LSTMs outputs'''
        return K.exp(-K.sum(K.abs(left-right), axis=1, keepdims=True))
    
    # The visible layer
    left_input = Input(shape=(max_seq_length,), dtype='int32')
    right_input = Input(shape=(max_seq_length,), dtype='int32')

    embedding_layer = Embedding(len(embeddings), embedding_dim, weights=[embeddings], input_length=max_seq_length, trainable=False)

    # Embedded version of the inputs
    encoded_left = embedding_layer(left_input)
    encoded_right = embedding_layer(right_input)

     # Since this is a siamese network, both sides share the same LSTM
    #shared_lstm = LSTM(hp.Int("hidden_layers", min_value=2,  max_value=50, step=2))
    shared_lstm = LSTM(14)
    left_output = shared_lstm(encoded_left)
    right_output = shared_lstm(encoded_right)

    # Calculates the distance as defined by the MaLSTM model
    malstm_distance = Lambda(lambda x: exponent_neg_manhattan_distance(x[0], x[1]), output_shape=lambda x: (x[0][0], 1))([left_output, right_output])

    # Pack it all up into a model
    malstm = Model([left_input, right_input], [malstm_distance])

    # Adadelta optimizer, with gradient clipping by norm
    #optimizer = SGD(learning_rate=hp.Float("learning_rate", min_value=0.01,  max_value=1, step=0.01),
       #             momentum=hp.Float("momentum", min_value=0.0,  max_value=1, step=0.05), 
          #          nesterov=False, name='SGD')
            
    optimizer = SGD(learning_rate=0.64,
                momentum=0.9, 
                nesterov=False, name='SGD')

    malstm.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['accuracy'])
    
    return malstm

class PredictCustomSentences(Callback):
    def on_epoch_end(self, epoch, logs):
        result = []
        input1 = "A Man with a blue Hat visits his brother on a sunny Day"
        input2 = "A Guy goes to his brother, while the Sun is shining and wearing a blue hat"
        result.append(self.check_if_equal(input1, input2))

        input1 = "A Man with a blue Hat visits his brother on a sunny Day"
        input2 = "A woman steals a Football"
        result.append(self.check_if_equal(input1, input2))

        input1 = "A Man with a blue Hat visits his brother on a sunny Day"
        input2 = "A blue man is walking on Sun Avenue"
        result.append(self.check_if_equal(input1, input2))

        input1 = "A Man with a blue Hat visits his brother on a sunny Day"
        input2 = "A girl with a blue dress visits her brother on Sunday"
        result.append(self.check_if_equal(input1, input2))

        input1 = "A Man with a blue Hat visits his brother on a sunny Day"
        input2 = "On a sunny day, a man visits his brother, wearing a blue hat"
        result.append(self.check_if_equal(input1, input2))

        print("1:{}  ||  0:{}  ||  0:{}  ||  0.4:{}  ||  1:{}".format(result[0], result[1], result[2], result[3], result[4]))
        
        
    def check_if_equal(self, input1, input2):
        input1 = self.wordToNumber(input1)
        input2 = self.wordToNumber(input2)

        input1 = np.array([input1])
        input2 = np.array([input2])

        input1 = pad_sequences(input1, maxlen=max_seq_length)
        input2 = pad_sequences(input2, maxlen=max_seq_length)

        result =  self.model.predict([input1, input2])
        return result

    def wordToNumber(self, question):
        q2n = []  # q2n -> question numbers representation
        for word in text_to_word_list(question):
            # Check for unwanted words
            if word not in vocabulary:
                continue
            else:
                q2n.append(vocabulary[word])
        return q2n\

# Model variables
gradient_clipping_norm = 1.25
given_batch_size = 512
n_epoch = 30 # How often the complete Dataset run througth
training_start_time = time()
n_hidden = 20

LOG_DIR = "logs\{}".format(time())

tuner = RandomSearch(
    build_model,
    objective='val_accuracy',
    max_trials=2,
    executions_per_trial=2,
    directory=LOG_DIR)

print("start training")
tensorboard = TensorBoard(log_dir=LOG_DIR)
modelcheckpoint = ModelCheckpoint('model_weights.h5', save_weights_only=True)
pred_sen = PredictCustomSentences()

tuner.search(x = [np.array(X_train['left']), np.array(X_train['right'])],
             y = Y_train,
             epochs=120,
             batch_size=given_batch_size,
             validation_data=([np.array(X_validation['left']), np.array(X_validation['right'])], np.array(Y_validation)))
print("Training time finished in {}".format(datetime.timedelta(seconds=time()-training_start_time)))



# malstm = build_model(n_hidden, gradient_clipping_norm)
