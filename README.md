# Keras-Tuner-Search

In dieser Arbeit geht es um die Erstellung eines Systems, welches mit Hilfe künstlicher Intelligenz zwei Sätze semantisch auf deren Gleichheit prüft.

Ziel der Arbeit ist es, einen möglichst **präzisen Wert für die Genauigkeit** des LSTM zu erreichen.

Zudem geht es um das **Testen und Optimieren** eines LSTM-basierten neuronalen Netzes. Sie werden verstehen wie **neuronale Netze** aufgebaut sind und funktionieren. 
Außerdem werden einzelne Code-Bausteine und **Algorithmen** der neuronalen Netze und zugehörige **Optimierer** vorgestellt.

Diese Arbeit ist eine Weiterführung der Studienarbeit „Automatisiertes Feedback zur Unterstützung des Lernprozesses an Hochschulen und Universitäten“, verfasst von S. Sachse und P. Stehling. 
Der bereits bestehende Ansatz zur semantischen Texterfassung und zum Vergleich des Inhalts wurde an einigen Stellen verbessert, aktualisiert oder umfassend erneuert.